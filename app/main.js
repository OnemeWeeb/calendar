'use strict'
const namesOfMonths = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
const daysOfWeek = ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС'];
const daysOfMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
const date = new Date();
let year = parseInt(date.getFullYear());
let month = parseInt(date.getMonth());
let day, action;

$(function() {
    addCells(); //Creating items for days and weeks
    refreshCalendar(); //Filling calendar content
    gotEventOfTheDay();
});

const gotEventOfTheDay = () => {
    $('.day').click(function() { clickOnDay(this); });
    $('.arrow').click(function() { refreshCalendar(this); });
    $('#add').click(function() { getEventOfTheDay(this); });
    $('#change').click(function() { getEventOfTheDay(this); });
    $('#remove').click(function() { getEventOfTheDay(this); });
    $('#close').click(closeWindow);
}

const closeWindow = () => {
    $('#dialog_window').css('visibility', 'hidden');
    $('#event').html('');
    $('#string').val('');
}

const clickOnDay = (element) => {
    if ($(element).hasClass('other_days') === true) {
        day = $(element).html();
        refreshCalendar(element);
        $('.current_days:contains(' + day + '):first').toggleClass('active');
        $('#dialog_window').css('visibility', 'visible');
    } else if ($(element).hasClass('active') === true) {
        $(element).toggleClass('active');
        $('#dialog_window').css('visibility', 'hidden');
        day = $(element).html();
    } else {
        $('.day').toggleClass('active', false);
        $(element).toggleClass('active');
        $('#dialog_window').css('visibility', 'visible');
        day = $(element).html();
    }

    getEventOfTheDay(element);
}

const getEventOfTheDay = (element) => {
    let string = $('#string').val();
    let date = { year, month, day };

    if ($(element).attr('id') === 'add') {
        if (action === 'change') {
            action = 'remove and add';
        } else {
            action = 'add';
        }
    } else if ($(element).attr('id') === 'change') {
        action = 'change';
    } else if ($(element).attr('id') === 'remove') {
        action = 'remove';
    } else if ($(element).hasClass('day')) {
        action = 'get';
    }

    if (string === undefined || string === ' ') { string = ''; }

    let options = {
        type: 'POST',
        url: 'http://API:80/script.php',
        data: { date: date, string: string, action: action }
    };

    $.ajax({
        type: options.type,
        url: options.url,
        data: options.data
    }).done((a) => {
        let response = $.parseJSON(a);
        let $string = response.result.join(' \n');
        if (action === 'change') {
            $('#string').val($string);
            $('#event').html('');
        } else if (action === 'add' || action === 'remove') {
            $('#event').html($string);
            $('#string').val('');
        } else {
            $('#event').html($string);
            $('#string').val('');
        }

        $('#status').html(response.status);
    });
}

const refreshCalendar = (element) => { //Updating Calendar Content
    let idOfElement;

    if (element !== undefined) {
        if ($(element).hasClass('prev_month')) {
            idOfElement = 'month_prev';
        } else if ($(element).hasClass('next_month')) {
            idOfElement = 'month_next';
        } else {
            idOfElement = $(element).attr('id');
        }

        if (idOfElement === 'year_prev') { //Going to the previous year
            year--;
        } else if (idOfElement === 'year_next') { //Going to the next year
            year++;
        } else if (idOfElement === 'month_prev') { //Going to the previous month
            if (month === 0) {
                month = 11;
                year--;
            } else {
                month--;
            }
        } else if (idOfElement === 'month_next') { //Going to the next month
            if (month === 11) {
                month = 0;
                year++;
            } else {
                month++;
            }
        }
    }

    const nameOfMonth = namesOfMonths[month];
    $('#month').html(nameOfMonth);
    $('#year').html(year);
    fillCurrentMonth(month);
}

const getDaysOfMonth = (month) => { //Getting the number of days in a month
    let value;
    if (month === 1 && year % 4 === 0) { //Checking for leap year and February
        value = 29;
    } else if (month === -1) { //Checking for a possible negative December value
        value = 31;
    } else {
        value = daysOfMonth[month]; //Getting the number of days of the current month
    }

    return value;
}

const fillCurrentMonth = (month) => { //Filling in the numbers of the current month
    const today = new Date();
    const numberOfDays = getDaysOfMonth(month); //Number of days of the current month
    let firstDayPosition = (new Date(year, month, 0)).getDay(); //Getting the position of the first day of the month on the calendar
    if (firstDayPosition === 0) { //Wrap from start position to next line
        firstDayPosition = 7;
    }

    let incrementPosition = firstDayPosition; //The incremented value of the day’s position on the calendar
    let decrementPosition = firstDayPosition - 1; //The decremented value of the day’s position on the calendar
    let nextMonthDay = 1; //The value of the day of the next month
    let prevMonthDay = getDaysOfMonth(month - 1); //The value of the day of the previous month
    for (let i = 0; i <= 41; i++) { //Clearing Calendar Field
        $('.day').eq(i).toggleClass('current_days other_days today active prev_month next_month', false).html('');
    }
    for (let i = firstDayPosition; i >= 1; i--) { //Filling part of the numbers of the previous month
        $('.day').eq(decrementPosition--).toggleClass('other_days prev_month', true).html(prevMonthDay--);
    }
    for (let i = 1; i <= 42; i++) {
        if (i <= numberOfDays) { //Filling in the numbers of the current month
            if (year === today.getFullYear() && month === today.getMonth() && i === today.getDate()) {
                $('.day').eq(incrementPosition++).toggleClass('current_days today', true).html(i);
            } else {
                $('.day').eq(incrementPosition++).toggleClass('current_days', true).html(i);
            }
        } else if (i <= 42 - firstDayPosition) { //Filling part of the numbers of the next month
            $('.day').eq(incrementPosition++).toggleClass('other_days next_month', true).html(nextMonthDay++);
        }
    }
}

const addCells = () => { //Creating items for days and weeks
    for (let i = 0; i <= 6; i++) {
        if (i === 0) { //Adding the days of the week
            $('<div>', { class: 'week', id: 'column' }).insertBefore('#days');

            for (let j = 0; j <= 6; j++) {
                $('<span>', { class: 'day_week', text: daysOfWeek[j] }).appendTo('#column');
            }
        } else { //Adding weeks and days of the month
            $('<div>', { class: 'week', id: 'row' + i }).appendTo('#days');

            for (let j = 1; j <= 7; j++) {
                $('<span>', { class: 'day' }).appendTo('#row' + i);
            }
        }
    }
}